<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/formidable/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'importer_formulaire_statut_label' => 'Statut des réponses importées',
	'message_champ_obligatoire_non_dispo' => 'Le champ @champ@ obligatoire n\'est pas présent dans votre import.',
	'message_fichier_sans_reponse' => 'Votre fichier ne semble pas contenir de réponses.',
	'message_formulaire_inexistant' => 'Le formulaire n\'existe pas.',
	'message_insertions_nb' => '@nb@ réponses ont été importées.',
	'reponses_importer' => 'Importer des réponses',

);